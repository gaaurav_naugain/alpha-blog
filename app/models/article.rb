class Article< ApplicationRecord
validates :title , presence: true , length: {minimum: 5, maximum: 100}
validates :descrption , presence: true , length: {minimum: 15, maximum: 1000}

end
